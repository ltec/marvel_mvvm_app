//
//  Config.swift
//  MarvelMvvmApp
//
//  Created by Ti Corporativo on 11/03/20.
//  Copyright © 2020 HomeTec. All rights reserved.
//

import Foundation
import HomeTecUtil

class Config {
    static let baseHost: String = "https://gateway.marvel.com/"
    private static let publicKey: String = "8c73cb9e29cfec1272b3b2b541be8567"
    private static let privateKey: String = "f255b5119000a3b27b7a1ca1a9d73f2f4186c2ce"
    
    static let baseKey: String = {
        let timestamp = NSDate().timeIntervalSince1970
        let hash = MD5.MD5Hex(string: "\(timestamp)\(Config.privateKey)\(Config.publicKey)")
        let key = "ts=\(timestamp)&apikey=\(Config.publicKey)&hash=\(hash)"
        return key
    }()
    
}
