//
//  Config.swift
//  HelpFitness
//
//  Created by Leandro Silva Andrade on 17/08/19.
//  Copyright © 2019 Leandro Silva Andrade. All rights reserved.
//

import Foundation

class Config {
    
    private static let configuration: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = false
        config.httpAdditionalHeaders = ["Content-Type":"application/json"]
        config.httpMaximumConnectionsPerHost = 5
        config.timeoutIntervalForRequest = 30.0
        return config
    }()
    
    public static let session = URLSession.shared //URLSession(configuration: configuration)
}

public enum MethodEnum: String {
    case Post = "POST"
    case Get = "GET"
    
    func getValue() -> String {
        return self.rawValue
    }
}

public enum StatusCodeEnum: Int {
    case Success = 200
    case BadRequest = 400
    case NoAuthorized = 401
    case Forbidden = 403
    case NotFound = 404
    case ServerError = 500
}
