//
//  File.swift
//  HelpFitness
//
//  Created by Leandro Silva Andrade on 17/08/19.
//  Copyright © 2019 Leandro Silva Andrade. All rights reserved.
//

import Foundation

public enum RequestServiceError: Error {
    case noNetwork
    case emptyResponse
    case emptyBody(response: URLResponse?)
    case emptyBodyParam
    case decodingError(error: DecodingError?)
    case emptyUrl
    case errorUrl
    case error(statusCode: StatusCodeEnum?)
    case errorEncodeBody
    case errorResponse(error: Error?)
    case emptyMethod
}
