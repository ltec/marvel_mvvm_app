//
//  MD5.swift
//  HomeTecUtil
//
//  Created by Ti Corporativo on 11/03/20.
//  Copyright © 2020 HomeTec. All rights reserved.
//

import Foundation
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

public class MD5 {

    private static func MD5Format(string: String) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)
        
        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
    
    public static func MD5Hex(string: String) -> String {
        let md5Data = MD5.MD5Format(string: string)
        
        let md5Hex =  md5Data.map { String(format: "%02hhx", $0) }.joined()
        return md5Hex
    }
    
    public static func MD5Base64(string: String) -> String {
        let md5Data = MD5.MD5Format(string: string)
        
        let md5Base64 = md5Data.base64EncodedString()
        return md5Base64
    }
}
